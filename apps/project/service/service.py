import requests

from apps.project.models import Project


class GetAllProjectsContributorsDataFromGitlab:
    @staticmethod
    def get_data():
        projects_queryset = Project.objects.all()
        projects_contributors = []
        for project in projects_queryset:
            projects_contributors_url = f"""\
                            https://gitlab.weem.internal/api/v4/projects/{project.gitlab_id}/repository/contributors"""
            contributors_data = requests.get(projects_contributors_url).json()
            projects_contributors.append(
                {"project_id": project.id, "project_name": project.name, "contributors_data": contributors_data})

        return projects_contributors
