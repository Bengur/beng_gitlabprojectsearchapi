from apps.project.models import Contributor
from apps.project.service.service import GetAllProjectsContributorsDataFromGitlab


class CreateUpdateProjectContributors:
    @staticmethod
    def create():
        projects_with_contributors_data = GetAllProjectsContributorsDataFromGitlab.get_data()
        contributors_queryset = Contributor.objects.all()
        for project in projects_with_contributors_data:
            for project_contributor in project["contributors_data"]:
                flag = False
                for contributor in contributors_queryset:
                    if contributor.email == project_contributor["email"]:
                        flag = True
                        break
                if flag is False:
                    Contributor.objects.get_or_create(email=project_contributor["email"])
