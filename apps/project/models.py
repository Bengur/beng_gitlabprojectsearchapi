from django.db import models


class Project(models.Model):
    gitlab_id = models.PositiveIntegerField()
    name = models.CharField(max_length=150)
    description = models.TextField(max_length=1000, null=True, blank=True)
    created_at = models.CharField(max_length=50)
    tag_list = models.TextField(max_length=200)
    web_url = models.TextField(max_length=200)

    def __str__(self):
        return self.name


class Contributor(models.Model):
    email = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.email


class ProjectContributorRelation(models.Model):
    commits = models.PositiveIntegerField()
    additions = models.PositiveIntegerField()
    deletions = models.PositiveIntegerField()
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    contributor = models.ForeignKey(Contributor, on_delete=models.CASCADE)
