from rest_framework import generics

from apps.project.models import Contributor
from apps.project.serializers.contributor_serializer import ContributorSerializer


class RetrieveContributor(generics.RetrieveAPIView):
    def get_queryset(self):
        return Contributor.objects.filter(id=self.kwargs['pk'])

    serializer_class = ContributorSerializer
