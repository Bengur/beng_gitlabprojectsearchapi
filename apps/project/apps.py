from django.apps import AppConfig


class ProjectserviceConfig(AppConfig):
    name = 'apps.project'
