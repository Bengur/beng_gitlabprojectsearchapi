from django.contrib import admin
from django.contrib.auth.models import Group

from apps.project.models import Project, Contributor, ProjectContributorRelation

admin.site.unregister(Group)


class ProjectAdmin(admin.ModelAdmin):
    list_display = ["name", "id", "gitlab_id"]
    search_fields = ["name", "description", "tag_list", "web_url"]


admin.site.register(Project, ProjectAdmin)


class ContributorAdmin(admin.ModelAdmin):
    list_display = ["email", "id"]
    search_fields = ["email"]


admin.site.register(Contributor, ContributorAdmin)


class ProjectContributorRelationAdmin(admin.ModelAdmin):
    list_display = ["id", "project", "contributor", "commits", "additions", "deletions"]


admin.site.register(ProjectContributorRelation, ProjectContributorRelationAdmin)
