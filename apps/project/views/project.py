from rest_framework import generics

from apps.project.models import Project
from apps.project.serializers.project_serializer import ProjectSerializer


class RetrieveProject(generics.RetrieveAPIView):
    def get_queryset(self):
        return Project.objects.filter(id=self.kwargs['pk'])

    serializer_class = ProjectSerializer