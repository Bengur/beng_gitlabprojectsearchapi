##gitlab-api Api Development Guide

###For apache configuration

    sudo apt-get install libapache2-mod-wsgi-py3

##Development setup

###Install required system packages:

    sudo apt-get install python3-pip
    sudo apt-get install python3-dev python3-setuptools
    sudo apt-get install libpq-dev
    sudo apt-get install postgresql postgresql-contrib
    sudo apt-get install binutils libproj-dev gdal-bin

###Create www directory where project sites and environment dir

    mkdir /var/www && mkdir /var/envs && mkdir /var/envs/bin

###Install virtualenvwrapper

    sudo pip3 install virtualenvwrapper
    sudo pip3 install --upgrade virtualenv

###Add these to your bashrc virutualenvwrapper work

    export WORKON_HOME=/var/envs
    export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
    export PROJECT_HOME=/var/www
    export VIRTUALENVWRAPPER_HOOK_DIR=/var/envs/bin    
    source /usr/local/bin/virtualenvwrapper.sh

###Create virtualenv

    cd /var/envs && mkvirtualenv --python=/usr/bin/python3 gitlab-api


###Install requirements for a project.

    cd /var/www/gitlab-api && pip install -r requirements.txt


##Database creation
###For psql

    sudo su - postgres
    psql > DROP DATABASE IF EXISTS gitlab_api;
    psql > CREATE DATABASE gitlab_api;
    psql > CREATE USER gitlab_api_user WITH password 'gt5rfje1dopp9v787qcf5gfiqx';
    psql > GRANT ALL privileges ON DATABASE gitlab_api TO gitlab_api_user;
    psql > ALTER USER gitlab_api_user CREATEDB;
