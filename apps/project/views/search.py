from rest_framework import generics, filters

from apps.project.models import Project
from apps.project.serializers.project_serializer import ProjectSerializer


class SearchProjectView(generics.ListAPIView):
    """
    searching fields are:
    -project description
    -project tags
    -project contributor id
    -project contributor email
    """
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = [
        "description",
        "tag_list",
        "projectcontributorrelation__contributor__id",
        "projectcontributorrelation__contributor__email"
    ]
