from apps.project.models import Project


class CreateUpdateProjects:
    @staticmethod
    def create_update(projects_data):
        projects_queryset = Project.objects.all()
        for project_data in projects_data:
            flag = False
            for project in projects_queryset:
                if project_data["id"] == project.gitlab_id:
                    setattr(project, "name", project_data["name"])
                    setattr(project, "description", project_data["description"])
                    setattr(project, "created_at", project_data["created_at"])
                    setattr(project, "tag_list", project_data["tag_list"])
                    setattr(project, "web_url", project_data["web_url"])
                    project.save()
                    flag = True
                    break
            if flag is False:
                Project.objects.create(gitlab_id=project_data["id"], name=project_data["name"],
                                       description=project_data["description"], created_at=project_data["created_at"],
                                       tag_list=project_data["tag_list"], web_url=project_data["web_url"])
