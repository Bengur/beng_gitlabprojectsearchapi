import requests


class GetProjectsDataFromGitlab:
    @staticmethod
    def get_data_by_user_id(gitlab_user_id):
        project_main_data_url = f"""https://gitlab.weem.internal/api/v4/users/{gitlab_user_id}/projects"""
        data = requests.get(project_main_data_url).json()
        return data
