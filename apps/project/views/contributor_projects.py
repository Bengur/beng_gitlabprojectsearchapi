from rest_framework import generics

from apps.project.models import Project
from apps.project.serializers.project_serializer import ProjectSerializer


class RetrieveContributorProjects(generics.ListAPIView):
    def get_queryset(self):
        return Project.objects.filter(projectcontributorrelation__contributor__id=self.kwargs['pk'])

    serializer_class = ProjectSerializer