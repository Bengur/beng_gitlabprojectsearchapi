from apps.project.models import ProjectContributorRelation, Project, Contributor
from apps.project.service.service import GetAllProjectsContributorsDataFromGitlab


class CreateUpdateProjectContributorRelations:
    @staticmethod
    def create_update():
        projects_with_contributors_data = GetAllProjectsContributorsDataFromGitlab.get_data()
        projects_queryset = Project.objects.all()
        contributors_queryset = Contributor.objects.all()
        for project in projects_with_contributors_data:
            for project_contributor in project["contributors_data"]:
                flag = False
                for relation in ProjectContributorRelation.objects.all():
                    if relation.project.id == project["project_id"] and relation.contributor.email == \
                            project_contributor["email"]:
                        setattr(relation, "commits", project_contributor["commits"])
                        setattr(relation, "additions", project_contributor["additions"])
                        setattr(relation, "deletions", project_contributor["deletions"])
                        relation.save()
                        flag = True
                        break
                if flag is False:
                    ProjectContributorRelation.objects.create(commits=project_contributor["commits"],
                                                              additions=project_contributor["additions"],
                                                              deletions=project_contributor["deletions"],
                                                              project=projects_queryset.get(id=project["project_id"]),
                                                              contributor=contributors_queryset.get(
                                                                  email=project_contributor["email"]))
