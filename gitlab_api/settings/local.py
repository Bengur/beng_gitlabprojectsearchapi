from gitlab_api.settings.base import *

DEBUG = True

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'gitlab_api',
        'USER': 'gitlab_api_user',
        'PASSWORD': 'gt5rfje1dopp9v787qcf5gfiqx',
        'HOST': '127.0.0.1',
        'PORT': '5432'
    }
}
