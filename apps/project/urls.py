from django.urls import path

from apps.project.views.contributor import RetrieveContributor
from apps.project.views.contributor_projects import RetrieveContributorProjects
from apps.project.views.project import RetrieveProject
from apps.project.views.project_contributors import RetrieveProjectContributors
from apps.project.views.search import SearchProjectView
from apps.project.views.synchronize_db_with_gitlab import SynchronizeDbWithGitlabView

urlpatterns = [
    path("sinchronize_db_with_gitlab/<int:token>/<int:gitlab_user_id>", SynchronizeDbWithGitlabView.as_view(),
         name="synchronize_db_with_gitlab"),
    path("projects/<int:pk>", RetrieveProject.as_view(), name="projects/project"),
    path("projects/contributors/<int:pk>",
         RetrieveProjectContributors.as_view(),
         name="projects/project/contributors"),
    path("contributors/<int:pk>", RetrieveContributor.as_view(),
         name="contributors/contributor"),
    path("contributors/projects/<int:pk>",
         RetrieveContributorProjects.as_view(),
         name="contributors/contributor/projects"),
    path("projects/search/", SearchProjectView.as_view())
]
