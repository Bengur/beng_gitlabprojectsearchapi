from django.db import transaction
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.project.service.create_update_contributors import CreateUpdateProjectContributors
from apps.project.service.create_update_projects import CreateUpdateProjects
from apps.project.service.create_update_relations import CreateUpdateProjectContributorRelations
from apps.project.service.get_projects_data_from_gitlab import GetProjectsDataFromGitlab


class SynchronizeDbWithGitlabView(APIView):
    @transaction.atomic
    def get(self, request, *args, **kwargs):
        # 5768478
        if self.kwargs["token"] == 12345:
            projects_data = GetProjectsDataFromGitlab.get_data_by_user_id(self.kwargs["gitlab_user_id"])
            CreateUpdateProjects.create_update(projects_data)
            CreateUpdateProjectContributors.create()
            CreateUpdateProjectContributorRelations.create_update()
            return Response("DB updated")

        return Response("Please provide valid token")
